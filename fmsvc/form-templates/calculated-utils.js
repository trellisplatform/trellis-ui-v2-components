// import { FORM_CALCULATED_ACTIONS, FORM_CALCULATED_FIELD_TYPE } from '@/constants'

export const getCalculatedValue = (inputTempl, apiServices, inputValues) => {
  if (!apiServices || !apiServices.getValueFromSection) {
    return ''
  }

  const ops = getOps(inputTempl.ops, apiServices, inputValues)

  let result = ''
  switch (inputTempl.action) {
    case 'multiply':
      result = ops[0] * ops[1]
      break
    case 'divide':
      result = ops[0] / ops[1]
      break
  }

  // process result
  if (result) {
    if (inputTempl.res && inputTempl.res.round) {
      switch (inputTempl.res.round) {
        case 'down':
          result = Math.floor(result)
          break
        case 'up':
          result = Math.ceil(result)
          break
      }
    } else {
      result = result.toFixed(2)
    }
  }
  return result
}

const getOps = (ops, apiServices, inputValues) => {
  return ops.map(op => {
    switch (op.type) {
      case 'const':
        // parse depending on the type
        return +op.value

      case 'var':
        // parse depending on the type
        return +apiServices.getValueFromSection(inputValues, op.sectionId, op.inputId)
    }
  })
}
